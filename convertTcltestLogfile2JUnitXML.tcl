# see http://help.catchsoftware.com/display/ET/JUnit+Format

set testSuiteData_fileName [lindex $argv 0]
set xml_filename [lindex $argv 1]

# Initial variables required for parseOutput 
set ::isTestOutput 0
set ::testOutput ""

proc parseOutput {line} {
    
    # Detect testcase begin
    set startSuffix [string first {---- } $line]
    set startPostfix [string last { start} $line]
    
    if { 0 == $startSuffix && 0 < $startPostfix } {
        
        set ::isTestOutput 1 
        set name [string trimleft $line {---- }]
        set name [string trimright $name {start}]
        set name [string trim $name]
        set ::testCaseName $name 
        set ::testOutput ""
        
    } elseif { [info exists ::testCaseName] && ( $line == "++++ $::testCaseName PASSED" || $line == "==== $::testCaseName  FAILED" ) } {
        # Detect testcase final
        
        set ::testOutputs($::testCaseName) $::testOutput
        set ::isTestOutput 0
        unset ::testCaseName
    
    } elseif  {$::isTestOutput == 1} {
        # Append output
        
        append ::testOutput "$line\n"
        
    }
}

proc parseTestSuite {file_data} {
    set testsuite_data [split $file_data \n]
	set timestampFromLogFile ""
    set testFiles [list]
	set testFile ""
	set className ""
	set testFile2Case [list]
	set xml_data [list]
	set testDirectory ""
	set executionDirectory ""
	set packageName ""
	set testFileMatchPattern ""
	set testFileMatchPatternLowerCase ""
		
    foreach line $testsuite_data {
        # Record standard output
        #parseOutput $line
		
        if {[regexp {^Sourced 0 Test Files.} $line]} {
			return ""
		}
		if {$testFileMatchPattern == "" && [regexp {^Only running test files that match:} $line]} {
			set testFileMatchPattern [lindex $line end]
			set testFileMatchPatternLowerCase [string tolower $testFileMatchPattern]
		}
        if {$testFileMatchPattern != "" && [regexp {^[a-zA-Z.]+$} $line] && [expr [regexp [subst -nocommands {^[a-zA-Z0-9.]$testFileMatchPattern}] $line] || [regexp [subst -nocommands {^[a-zA-Z0-9.]$testFileMatchPatternLowerCase}] $line]]} {
			lappend testFiles $line
			set testFile $line
        }
		if {[regexp {^\-\-\-\- (.*?) start} $line] || [regexp {^\+\+\+\+ (.*?) SKIPPED:} $line]} {
			set testCase [lindex $line 1]
			lappend testFile2Case "$testFile $testCase"
		}
		if {[regexp {^Tests began at} $line]} {
			# from: Tests began at Mon Dec 19 13:47:06 CET 2016
			# To:	2016-12-19T13:47:06
			set timestampFromLogFile [clock format [clock scan [lrange $line 3 end]] -format %Y-%m-%dT%H:%M:%S]
		}
		if {[regexp {^Tests located in:} $line]} {
			set testDirectory [lindex $line 3]
		}
		if {[regexp {^Tests running in:} $line]} {
			set executionDirectory [lindex $line 3]
			set packageName [regsub -all "/" [string range $testDirectory [expr [string length $executionDirectory]+1] end] "."]
		}
		regexp {^.*:\tTotal\t(\d+)\tPassed\t(\d+)\tSkipped\t(\d+)\tFailed\t(\d+)$} $line foo Total Passed Skipped Failed
    }
	if {$testFiles == ""} {
		return ""
	}
	append xml_data "\n\t<testsuite timestamp=\"$timestampFromLogFile\" name=\"$packageName\" tests=\"$Total\" passed=\"$Passed\" skipped=\"$Skipped\" failures=\"$Failed\">\n"
	foreach testFile $testFiles {
		set className [file rootname $testFile]
		set file_data_passed $file_data
		while {[regexp -indices {\+\+\+\+ ([a-zA-Z0-9_]+) PASSED} $file_data_passed indices_passed]} {    
			set passed_test [string range $file_data_passed [lindex $indices_passed 0] [lindex $indices_passed 1]]
			set file_data_passed [string range $file_data_passed [expr [lindex $indices_passed 1] + 1] end]
			set passedTestName [lindex $passed_test 1]
			if {[lsearch $testFile2Case "$testFile $passedTestName"] >= 0} {
				append xml_data "\t\t<testcase name=\"$passedTestName\" classname=\"$packageName.$className\">PASSED</testcase>\n"
			}
		}
		set file_data_skipped $file_data
		while {[regexp -indices {\+\+\+\+ ([a-zA-Z0-9_]+) SKIPPED: ([a-zA-Z]+)} $file_data_skipped indices_skipped]} {
			set skipped_test [string range $file_data_skipped [lindex $indices_skipped 0] [lindex $indices_skipped 1]]
			set file_data_skipped [string range $file_data_skipped [expr [lindex $indices_skipped 1] + 1] end]
			set skippedTestName [lindex $skipped_test 1]
			set reason [lindex $skipped_test end]
			if {[lsearch $testFile2Case "$testFile $skippedTestName"] >= 0} {
				append xml_data "\t\t<testcase name=\"$skippedTestName\" classname=\"$packageName.$className\"><skipped message=\"$reason\"/></testcase>\n"
			}
		}
		set file_data_failed $file_data
		while {[regexp -indices {==== (.*?) FAILED\n==== Contents of test case:((.|\n)*?)==== (.*?) FAILED} $file_data_failed indices]} {
			set error_code [string range $file_data_failed [lindex $indices 0] [lindex $indices 1]]
			set file_data_failed [string range $file_data_failed [expr [lindex $indices 1] + 1] end]
			set failedTestName [lindex [split $error_code \n] 0 1]
			if {[lsearch $testFile2Case "$testFile $failedTestName"] >= 0} {
				append xml_data "\t\t<testcase name=\"$failedTestName\" classname=\"$packageName.$className\">\n"
				
				if [regexp {(.?)---- Test generated error; } $error_code] {
					if [llength [set content [regexp -inline {==== Contents of test case:((.|\n)*?)---- Test generated error;} $error_code]]] {
						set content [lindex $content 1]         
					}
					if [llength [set errorInfo [regexp -inline {(.?)---- errorInfo:((.|\n)*?)---- errorCode:} $error_code]]] {
						set errorInfo [lindex $errorInfo 2]         
					}        
					if [llength [set errorCode [regexp -inline {(.?)---- errorCode:((.|\n)*?)==== (.*?) FAILED} $error_code]]] {
						set errorCode [lindex $errorCode 2]         
					}
					
					append xml_data "\t\t\t<error message=\"test error\">\n"
					append xml_data "errorInfo: $errorInfo"
					append xml_data "\t\t\t</error>\n"
					if { [info exists ::testOutputs($failedTestName)] } {
						append xml_data "\t\t\t<system-out>\n"
						append xml_data $::testOutputs($failedTestName)
						append xml_data "\t\t\t</system-out>\n"
					}
					append xml_data "\t\t</testcase>\n"
				} else {     
					append xml_data "\t\t\t<failure message=\"test failure\">\n"
				
					if [llength [set content [regexp -inline {==== Contents of test case:((.|\n)*?)---- Result was:} $error_code]]] {
						set content [lindex $content 1]         
					}
					if [llength [set result [regexp -inline {Result was:((.|\n)*?)---- Result should have been} $error_code]]] {
						set result [lindex $result 1]        
					}
					if [llength [set expectation [regexp -inline {Result should have been \(exact matching\):((.|\n)*?)==== (.*?) FAILED} $error_code ]]] {
						set expectation [lindex $expectation 1]
					}
					
					append xml_data "expected $expectation but got $result"
					append xml_data "\t\t\t</failure>\n"
					if { [info exists ::testOutputs($failedTestName)] } {
						append xml_data "\t\t\t<system-out>\n$::testOutputs($failedTestName)"
						append xml_data "\t\t\t</system-out>\n"
					}
					append xml_data "\t\t</testcase>\n"

				}
				set ::ANY_TEST_FAILED 1
			}
		}
	}
	append xml_data "\t</testsuite>"
    return $xml_data
}

proc wsplit {str sepStr} {
    if {![regexp $sepStr $str]} {
        return [list $str]}
    set strList {}
    set pattern (.*?)$sepStr
    while {[regexp $pattern $str match left]} {
        lappend strList $left
        regsub $pattern $str {} str
    }
    lappend strList $str
    return $strList
}

set fp [open $testSuiteData_fileName r]
set all_file_data [read $fp]
close $fp

set units_fileDataList [wsplit $all_file_data "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"]

set ANY_TEST_FAILED 0

#create xml file
set fileID [open $xml_filename "w"]

puts $fileID "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
puts -nonewline $fileID "<testsuites>"

foreach units_fileData $units_fileDataList {
	if {$units_fileData != "\n"} {
		set xml_data [parseTestSuite $units_fileData]
		if {$xml_data != ""} {
			puts $fileID $xml_data
		}
	}
}

puts -nonewline $fileID "</testsuites>"

close $fileID

# necessary to stop gradle build
if {$ANY_TEST_FAILED} {
    error "One or more tests failed"
}